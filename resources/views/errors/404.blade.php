@extends('app')

@section('title', '404')

@section('content')
<div class="container-fluid error-infobox">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Uh Oh!</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            <li>The page you are looking for could not be found.</li>
                        </ul>
                    </div>
                    <a href="{{ url('/') }}" type="button" class="btn btn-primary btn-sm">Back to Home</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection