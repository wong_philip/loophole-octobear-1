<h3>Health Seminars</h3>
<p class="meet-our-team">
    Our Health and Wellness Seminars and workshops provide an opportunity to introduce a wide range of topics to
    corporations, associations and individuals. Our wellness education series is designed to provide information and
    guidance allowing everyone to achieve a healthy and balanced lifestyle. On-going education and knowledge is the key
    to making more informed choices about health and well-being. The Team at Tsim Sha Tsui Chiropractic provides a host
    of educational workshops that can be delivered to groups of individuals, to employees as part of a lunchtime
    program, a training session, or a means of kicking off a seminar or corporate initiative. As a community service, we
    provide this service at no charge; our goal is to help our community to achieve total wellness.
</p>
<h4>Our Health Topics Include:</h4>

<ul>
    <li>What is True Wellness?</li>
    <li>Office Ergonomics</li>
    <li>Power Stretching</li>
    <li>How to raise Healthy Kids</li>
    <li>How to create an energetic workplace</li>
</ul>

For enquiries, please contact us at <a href="tel:27213999">2721-3999</a> or <a href="mailto:info@tstcc.hk">info@tstcc.hk</a>