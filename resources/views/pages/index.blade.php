@extends('app')

@section('title', 'Health & Spline')

@section('breadcrumbs', Breadcrumbs::render('index'))

@section('content')
    <h2>@yield('title')</h2>

    <div class="row">
        <div class="col-md-8">
            <h3>Our Mission</h3>

            <p>
                Our body consists of several systems; nerve system, organ system and musculoskeletal system. They work
                in a synchronized manner for us to express optimal health.
            </p>

            <p>
                We believe our nervous system plays an important role, it allows our body to adapt to the environment
                efficiently. The muscular and skeletal systems are equally important to protect our organs, allow
                locomotion, and bring our body to life. The organ system is crucial for nutrient absorption, toxin
                excretion, and effects our emotions greatly.
            </p>

            <p>
                We are a family Chiropractic Centre, and our focus is to maintain balance of all three systems- the
                nerve system, the musculoskeletal system, and organ system through chiropractic adjustments, nutritional
                advices, and proper spinal education.
            </p>

            <p>
                Listening to your concerns is our responsibility, getting your three systems checked is your
                responsibility.
            </p>

            <blockquote>
                <p>We are given one life, and the decision is ours whether to wait for circumstances to make up our
                    mind, or whether to act, and in acting, to live.</p>
                <footer><cite title="Source Title">General Omar N. Bradley (1893 -1981)</cite></footer>
            </blockquote>
            <blockquote>
                <p>Living at risk is jumping off the cliff and building your wings on the way down.</p>
                <footer><cite title="Source Title">Ray Bradbury (1920 - 2012)</cite></footer>
            </blockquote>
        </div>
    </div>
@endsection