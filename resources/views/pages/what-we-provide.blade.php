@extends('app')

@section('title', 'What We Provide')

@section('breadcrumbs', Breadcrumbs::render('what-we-provide'))

@section('content')
    <h2>@yield('title')</h2>

    <div class="row">
        <div class="col-md-9 tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="care">
                @include('pages.what-we-provide.care')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="seminars">
                @include('pages.what-we-provide.seminars')
            </div>
        </div>
        <div class="makeAGap hidden-lg"></div>
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" role="tablist" id="myTab">
                <li role="presentation" class="active">
                    <a href="#care" aria-controls="care" role="tab" data-toggle="pill">Chiropractic Care</a>
                </li>
                <li role="presentation">
                    <a href="#seminars" aria-controls="seminars" role="tab" data-toggle="pill">Health Seminars</a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('bottom-js')
    <script type="text/javascript" src="{{ asset('/assets/js/what-we-provide.js') }}"></script>
@endsection
