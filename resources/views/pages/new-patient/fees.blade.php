<h3>Fees</h3>

<p>Payment by Cash, Credit Card or American Express</p>

<p>
    The duration of an initial consultation with an Adjustment session (Treatment) is approximately 45 minutes, and the
    cost ranges between HK$420 to HK$840 (X ray fees, rehabilitation equipment... are not included).
</p>

<p>
    Questions? Please <a href="{{ url('/contact-us') }}" title="Contact Us">contact us</a> or visit the <a href="#" title="FAQ" id="navfaq2">Frequently Asked Questions</a> page.
</p>