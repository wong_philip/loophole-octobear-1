<h3>FAQ</h3>

<h4>Q: What is an adjustment?</h4>
<p class="answer">
    A: The key to chiropractic care is the spinal adjustment. This is the way doctors of chiropractic correct
    subluxations. The goal of the chiropractic adjustment is to correct the spinal subluxations detected during the
    examination. To do that, the doctor applies pressure to the bone and "releases" nerve interference. The body will
    then be recuperate itself over time.
</p>

<h4>Q: Does it hurt to get adjusted?</h4>
<p class="answer">
    A: Chiropractic adjustments allow the nerve exiting the spine to be interference free, therefore stress and tension
    of the body will be reduced.
    Adjustments are gentle, yet effective, treatment.
</p>

<h4>Q: Is chiropractic care safe?</h4>
<p class="answer">
    A: Yes. Surveys from various countries found that chiropractic adjustments are very safe and boast a remarkably safe
    track record. According to statistics, chiropractic adjustments are 100 times safer than taking an over-the-counter
    pain reliever. Chiropractic is one of the safest types of health care in the world.
</p>

<h4>Q: Why do chiropractic adjustments produce a "cracking" sound?</h4>
<p class="answer">
    A: Lubricating fluids separate your spinal joints. Once you have been adjusted by a chiropractor, the gas and fluids
    in these joints can shift, thereby producing this popping sound as the gas is released.
</p>

<h4>Q: Can I crack the bones myself?</h4>
<p class="answer">
    A: <strong>DON'T TRY TO DO THIS!</strong> In fact, this can do more harm than good. Although you can crack your
    bones and hear the same
    popping sound, you will most likely be cracking the wrong joint in the wrong direction, which is already weak, thus
    making it even weaker. Adjustments need to be specific--they require a qualified chiropractor who has thoroughly
    examined you to locate the specific areas where you need the adjustment.
</p>

<h4>Q: How Long Will I Need Chiropractic Care?</h4>
<p class="answer">
    A: Ongoing supportive Chiropractic care will help with conditions that have been neglected from as long ago as early
    childhood. These conditions can be caused by muscle weakness, soft tissue damage, and degeneration of the spine.
    Most patients find that regular chiropractic check-ups help keep their bodies in an optimal status. Those who are
    active, have stressful jobs, or want to be their very best, discover that regularly scheduled preventive visits are
    helpful in the maintenance of good health.
</p>
<p class="answer">
    Only when their ache or pain becomes unbearable, do some patients seek chiropractic care. Unfortunately, this style
    of 'crisis management' often becomes more costly and time-consuming in the long run.
    The decision to keep benefiting from chiropractic care is inevitably up to you.
    Periodic chiropractic check-ups-- like brushing and flossing your teeth, getting regular exercise, and eating
    nutritious food-- are part of a healthy lifestyle.
</p>

<h4>Q: How does an adjustment feel and how long will it take?</h4>
<p class="answer">
    A: A lot of patients said that they enjoy their adjustments. Although in some cases, there have been individuals who
    have experienced tiredness or slight muscle aches after the first adjustment. For most patients, an adjustment might
    take only a few minutes, but the vital energy it releases can last for hours, days, or even weeks, depending on the
    stage of your chiropractic care.
</p>

<h4>Q: Can I be adjusted if I am pregnant?</h4>
<p class="answer">
    A: Absolutely! To ensure that the baby comes out healthy and in good posture, it is essential to have an adjustment
    prior to giving birth. Also, reliable studies have shown that pre-natal chiropractic care has helped to
    significantly reduce the pain in childbearing.
</p>

<h4>Q: Why do my children need chiropractic care?</h4>
<p>
    A: Children often experience their first subluxation at birth. Trauma from delivery can affect the soft,
    under-developed spine of a newborn. It is estimated that as much as 80lbs of pulling pressure can be exerted to
    remove a baby from the birth canal. Then, as children grow up and become more adventurous, the number of injuries to
    their spine becomes greatly increased. The repeated falls as children learn to walk and the running, jumping,
    twisting, bumping and falling all over the place will often cause your child to become subluxated.
</p>
<p class="answer">
    However, children seldom show any signs or symptoms of being subluxated until later on in life.
    Through consistent chiropractic care, your child can have a more responsive body, a more balanced flow of energy and overall increased performance as
    he/she passes through their stages of development. The risk of future health issues can be greatly reduced by having
    a subluxation-free child.
</p>