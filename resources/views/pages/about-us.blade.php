@extends('app')

@section('title', 'About Us')

@section('breadcrumbs', Breadcrumbs::render('about'))

@section('content')
    <h2>@yield('title')</h2>

    <div class="row">
        <div class="col-md-9 tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="meet">
                @include('pages.about-us.meet-our-team')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="testimonials">
                @include('pages.about-us.testimonials')
            </div>
        </div>
        <div class="makeAGap hidden-lg"></div>
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" role="tablist" id="myTab">
                <li role="presentation" class="active">
                    <a href="#meet" aria-controls="meet" role="tab" data-toggle="pill">Meet Our Team</a>
                </li>
                <li role="presentation">
                    <a href="#testimonials" aria-controls="testimonials" role="tab" data-toggle="pill">Patient
                        Testimonials</a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('bottom-js')
    <script type="text/javascript" src="{{ asset('/assets/js/about-us.js') }}"></script>
@endsection
