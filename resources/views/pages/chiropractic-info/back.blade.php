<h3>Backpain and Chiropractic care</h3>
<p class="lead">
    The nerve-rich facet joints on the back side of each spinal bone are a common cause of back pain. If
    these pairs of interlocking "fingers" aren't moving right, they can cause painful symptoms.</p>

<p>
    The discs between spinal bones can be a source of back pain, too. These rings of fibrous tissues act as spacers,
    connectors and "shock absorbers" for the spine. Trauma can cause the soft, pulpy material in the middle to bulge or
    herniate, putting pressure on delicate nearby nerves.
</p>
<p>
    Once you know the cause, you have several choices. One approach is bed rest. However, research shows that prolonged
    bed rest can actually delay recovery and make the problem worse!
</p>
<p>
    Physical therapy is another option. Still, exercising spinal joints that aren't working right is like continuing to
    drive your car with misaligned tires!
</p>
<p>
    Another choice is to numb or cover up the pain with drugs. While drugs can offer temporary relief, they can't
    correct functional problems of affected spinal joints.
    The most drastic measure is surgery. A laminectomy cuts off the offending facet joints, leaving the spinal cord
    exposed! A spinal fusion cuts out the disc tissue and immobilizes the joint. While there may be times when surgery
    makes sense, it's expensive, risky and more than half of all back surgeries fail.
</p>
<p>
    More and more people are choosing chiropractic care first. Specific chiropractic adjustments can help improve spinal
    function.
</p>
<p>
    Better yet, chiropractic care is safe. Chiropractic is natural. And chiropractic looks to correct the underlying
    cause of the problem.
</p>
