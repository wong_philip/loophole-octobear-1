<h3>Children and Chiropractic</h3>
<p>
    With pregnancy come hormonal changes, a shift in body weight and a laxity of ligaments. New stress is experienced by
    the lower back and pelvis. Many women report that their pregnancies were easier and their delivery times made
    shorter when they received chiropractic care throughout their pregnancy. Even today's birthing procedures can put
    tremendous pressure on a baby's spine. And while the injury from a forceps delivery may be less common today, vacuum
    extraction and eager hands can do even more damage by pulling, forcing and twisting the baby's young spine.
</p>
<p>
    Without the language to explain, many newborns experience colic, unexplained crying, lack of appetite, frequent ear
    infections or other signs of poor health.
    If these things are detected, a gentle, life-affirming adjustment is given. With no more pressure than you'd use to
    test the ripeness of a tomato, nerve-compromising subluxations can be reduced. Almost miraculously, many parents see
    instant improvement in the well-being of their child.
</p>
<p>
    Additionally, regular chiropractic checkups are helpful to monitor spinal development as infants sit upright, support
    their heads, learn to crawl, and take their first brave steps.
</p>
<p>
    Many experts believe that uncorrected spinal problems during this early stage of development cause the chronic,
    hard-to-correct subluxations seen in many adults.
    Sadly, bedwetting and many childhood aches and pains are passed off as "growing pains" or "just a phase they're
    going through." Beware! Aches and pains at any age are a sign that something isn't right. Subluxations may be
    involved.
</p>
<p>
    As you make decisions about your baby's health care and are confronted with the issues of antibiotics, vaccinations
    and the growing use of behaviour altering drugs, consult with your chiropractor. Seek accurate information and make
    an informed choice. Above all, make sure your child has the best chance to be all that he or she can be by having a
    nervous system free of subluxations.
</p>