@extends('app')

@section('title','Contact Us')

@section('breadcrumbs', Breadcrumbs::render('contact-us'))

@section('content')
    <h2>@yield('title')</h2>

    <div class="row">
        <div class="col-md-9">
            <h3>Details</h3>
            <address>
                Room 1201B<br/>
                12/F, Hong Kong Pacific Centre<br/>
                28 Hankow Road<br/>
                Tsim Sha Tsui<br/>
                Kowloon<br/>
                <abbr title="Phone">P:</abbr> (+852) 2721 3999
            </address>
            <h3>Available Time for Booking</h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td>Day</td>
                    <td>Time</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Mondays - Fridays</td>
                    <td>10:00AM-1:30PM, 3:30PM-6:30PM</td>
                </tr>
                <tr>
                    <td>Saturday</td>
                    <td>
                        10AM-1:30PM, 3:30PM-6:00PM
                    </td>
                </tr>
                <tr>
                    <td>Sundays & Public Holidays</td>
                    <td>
                        Closed
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-3">
            <div class="well well-sm" id="googleMapWell">
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d461.4367020804034!2d114.17128331779105!3d22.297164028695533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x068af0343503489c!2z5Lqe5aSq5Lit5b-D!5e0!3m2!1szh-TW!2shk!4v1427878733101"
                            width="600" height="450" frameborder="0" style="border:0"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection