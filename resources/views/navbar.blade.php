<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand brand" href="{{ url('/') }}">
                <img alt="Brand" class="brand" src="{{ asset('/assets/images/logoFull.png') }}">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}" title="Health & Spline">Health & Spline</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About
                        Us <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/about-us#meet') }}" id="navmeet">Meet Our Team</a></li>
                        <li><a href="{{ url('/about-us#testimonials') }}" id="navtest">Patient Testimonials</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">What
                        We Provide<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/what-we-provide#care') }}" id="navcare">Chiropractic Care</a></li>
                        <li><a href="{{ url('/what-we-provide#seminars') }}" id="navsemi">Health Seminars</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/news') }}" title="News">News</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">New Patient <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/new-patient#what-to-except') }}" id="navwte">What to Except?</a></li>
                        <li><a href="{{ url('/new-patient#fees') }}" id="navfees">Fees</a></li>
                        <li><a href="{{ url('/new-patient#faq') }}" id="navfaq">FAQ</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/chiropractic-info') }}" title="Chiropractic and You">Chiropractic and You</a>
                </li>
                <li><a href="{{ url('/contact-us') }}" title="Contact Us">Contact Us</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ url('/auth/login') }}">Login</a></li>
                    <li><a href="{{ url('/auth/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>